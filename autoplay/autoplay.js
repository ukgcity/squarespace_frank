<script>
    var intervalID = 0;

    YUI().use('history-hash', function(Y) {
        Y.on('hashchange', function(e) {
            setupAutoplay();
        }, Y.config.win);
    });

    Y.on("domready", function() {
        setupAutoplay();
    });

    function setupAutoplay() {
        if (Y.all(".sqs-gallery-design-grid-slide")) {
            setTimeout(function() {
                Y.all(".sqs-gallery-design-grid-slide img").on('click', function(e) {
                    contentNode = "";
                    if (!Modernizr.touch) {
                       contentNode += "<div style='padding: 2px; color: white; right: 5%; position: fixed; z-index: 100000002; width: 100px; top: 90%;' class='sqs-audio-player ready-state-initialized sqs-hide-title'>";
                       contentNode += "<div class='sqs-audio-player-content' style='padding-left:20px !important'>";
                       contentNode += "<div class='controls'></div>";
                       contentNode += "<div class='sqs-hide-title-text' style='white-space:nowrap;'>Hide titles</div>";
                       contentNode += "</div>";
                       contentNode += "</div>";
                    }
                    contentNode += "<div style='padding: 2px; color: white; right: 5%; position: fixed;";
                    if (!Modernizr.touch) {
                      contentNode += "top: 82%;";
                    } else {
                      contentNode += "top: 90%;";
                    }
                    contentNode += "z-index: 100000002; width:100px;' class='sqs-audio-player play-state-stopped ready-state-initialized lightbox-autoplay'>";
                    contentNode += "<div class='sqs-audio-player-content'>";
                    contentNode += "<div class='controls'></div>";
                    contentNode += "<div>Autoplay</div>";
                    contentNode += "</div>";
                    contentNode += "</div>";
                    Y.all(".yui3-lightbox2 .yui3-lightbox2-content").insert(contentNode, "after");
                    Y.all(".yui3-lightbox2-content").on('click', function(e) {
                        hideTitle();
                        setEvents();
                    });
                    Y.all(".sqs-lightbox-previous").on('click', function(e) {
                        hideTitle();
                        setEvents();
                    });
                    Y.all(".sqs-lightbox-next").on('click', function(e) {
                        hideTitle();
                        setEvents();
                    });

                    setEvents();

                    Y.all(".lightbox-autoplay").on('click', function(e) {
                        if (intervalID == 0) {
                            ap_items_count = Y.all(".sqs-lightbox-slideshow > div").size();
                            ap_item_width = parseInt(Y.all(".sqs-lightbox-slideshow > div:first-of-type").getStyle("width"));
                            intervalID = setInterval(function() {
                                if (Modernizr.touch) {
                                    if (!Y.all(".sqs-lightbox-slideshow")) {
                                        stopAutoplay();
                                    }
                                    ap_lightbox_left = parseInt(Y.all(".sqs-lightbox-slideshow").getStyle("left"));
                                    if (ap_lightbox_left == (-1 * (ap_items_count - 1) * ap_item_width)) {
                                        ap_lightbox_left = 0;
                                    } else {
                                        ap_lightbox_left -= ap_item_width;
                                    }
                                    Y.all(".sqs-lightbox-slideshow").setStyle("left", ap_lightbox_left);
                                } else {
                                    Y.one(".sqs-lightbox-next").simulate("click");
                                }
                                setEvents();
                            }, 5000);
                            Y.all(".lightbox-autoplay").removeClass("play-state-stopped").addClass("play-state-playing");
                        } else {
                            clearInterval(intervalID);
                            intervalID = 0;
                            Y.all(".lightbox-autoplay").removeClass("play-state-playing").addClass("play-state-stopped");
                        }
                    });
                    Y.all(".sqs-hide-title").on('click', function(e) {
                        changeTitleMode();
                        hideTitle();
                    });
                    Y.all(".sqs-lightbox-close").on('click', function(e) {
                        closeHideTitle();
                        stopAutoplay();
                    });
                });

            }, 2000);
        }
    }

    function stopAutoplay() {
        clearInterval(intervalID);
        intervalID = 0;
        Y.all(".lightbox-autoplay").removeClass("play-state-playing").addClass("play-state-stopped").hide();
    }

    function hideTitle() {
        if (Y.all(".sqs-hide-title .sqs-hide-title-text").get("text") == "Show titles") {
            Y.all(".sqs-lightbox-meta").setStyle("display", "none");
            Y.all(".sqs-lightbox-meta").removeClass("overlay-description-visible");
        } else {
            Y.all(".sqs-lightbox-meta").setStyle("display", "");
            Y.all(".sqs-lightbox-meta").addClass("overlay-description-visible");
        }
    }

    function changeTitleMode() {
        if (Y.all(".sqs-hide-title .sqs-hide-title-text").get("text") == "Hide titles") {
            Y.all(".sqs-hide-title .sqs-hide-title-text").set("text", "Show titles");
        } else {
            Y.all(".sqs-hide-title .sqs-hide-title-text").set("text", "Hide titles");
        }
    }

    function closeHideTitle() {
        Y.all(".sqs-hide-title").remove();
    }

    function setEvents() {
        Y.all(".sqs-lightbox-meta").each(function() {
           this.previous().on('mouseenter', function(e) { hideTitle(); });
           this.previous().on('mouseleave', function(e) { hideTitle(); });
           this.on('mouseenter', function(e) { hideTitle(); });
           this.on('mouseleave', function(e) { hideTitle(); });
        });
    }

    Y.one("body").on("keypress", function(e) {
        if (e.keyCode == 27) {
            if (Y.one(".sqs-lightbox-slideshow")) {
                closeHideTitle();
                stopAutoplay();
            }
        }
    });
</script>