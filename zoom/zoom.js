<script type="text/javascript">

$(window).on('hashchange', function(){
  setZoomChange();
});

 
/* ---------- ZOOM ---------- */
function setZoomChange() {
  setTimeout(function(){
	Y.all("#project img[src*='_zoom_image']").each(function (img_o){
		img_o.ancestor("div").addClass("item");
		img_o.ancestor("div").ancestor("div").setAttribute("id", "zoom_el");
		img_o.ancestor("div").insert('<a style="overflow:hidden;"><div class="wrapper"><div class="project-title"><h2>' + img_o.getAttribute("alt").replace("_zoom_image", "") + '</h2><h3>&mdash; view &mdash;</h3></div></div></a>',img_o);
	});
	if (!Modernizr.touch) {
        $("#zoom_el .item > img").each(function() {
		$(this).parent().find(".project-title").height($(this).height());
	});
    }
  },2000);
}
  
var cUrl		= decodeURIComponent(window.location.href);
cUrl			= cUrl.replace("http://","");
var path_arr		= cUrl.split("/");

if(!path_arr[path_arr.length-1]) {
	path_arr.splice(path_arr.length-1,1);
}

(function(open) {
	XMLHttpRequest.prototype.open = function(method, url, async, user, pass) {
		this.addEventListener("readystatechange", function() {
			 if("/" + path_arr[path_arr.length-1] + "/"==url && this.readyState==4) {
			  setZoomChange();
			 }
		}, false);
		open.call(this, method, url, async, user, pass);
	};
})(XMLHttpRequest.prototype.open);
/* --------- ZOOM --------- */

Y.use('node','squarespace-gallery-ng', function(Y) {

  window.Site = Singleton.create({

    ready: function() {
      this.listeners = [];

      Y.on('domready', this.initialize, this);
    },

    initialize: function() {
      this.ProjectEl = Y.one('#project');

      if (this.ProjectEl) {
        this._setupProject();
      }

    },

    _setupProject: function() {
      
      this.ProjectEl.all('.item img').each(function(img) {
        img.removeAttribute('alt');
      });
            

      this._setupProjectHovers(this.ProjectEl);
    },

    _setupProjectHovers: function(Project) {
      if (!Modernizr.touch) {
        if (Modernizr.csstransforms) {
          Project.delegate('mouseenter', Y.bind(this._onMouseEnter, this), '.item');

          Project.delegate('mousemove', Y.bind(this._onMouseMove, this), '.item');

          Project.delegate('mouseleave', Y.bind(this._onMouseLeave, this), '.item');
        } else {
          Project.delegate('mouseenter', function(e) {
            e.currentTarget.addClass('hovering');
            e.currentTarget.one('img').setStyles({
              'opacity': .22,
              'filter': 'alpha(opacity=22)'
            });
            e.currentTarget.one('div').setStyles({
              'opacity': 1,
              'filter': 'alpha(opacity=100)'
            });
          }, '.item');

          Project.delegate('mouseleave', function(e) {
            e.currentTarget.removeClass('hovering');
            e.currentTarget.one('img').setStyles({
              'opacity': 1,
              'filter': 'alpha(opacity=100)'
            });
            e.currentTarget.one('div').setStyles({
              'opacity': 0,
              'filter': 'alpha(opacity=0)'
            });
          }, '.item');

          Project.delegate('mousemove', function(e) {});
        }
      }    
    },

    _onMouseEnter: function(e) {
      var _this = this;

      clearTimeout(e.currentTarget.getAttribute('overlayDelay'));
      clearInterval(e.currentTarget.getAttribute('tickInterval'));

      e.currentTarget.setAttribute('overlayDelay', setTimeout(function() {
        e.currentTarget.addClass('hovering');
      }, 20));

      if (Y.Squarespace.Template.getTweakValue('project-hover-panning') + "" === "true") {
        e.currentTarget.setAttribute('tickInterval', setInterval(function() {
          e.currentTarget.one('img').tick();
        }, 10));
        e.currentTarget.setAttribute('original-top', e.currentTarget.one('img').getStyle('top'));
        e.currentTarget.setAttribute('original-left', e.currentTarget.one('img').getStyle('left'));
      }
    },

    _onMouseMove: function(e) {
      if (Y.Squarespace.Template.getTweakValue('project-hover-panning') + "" === "false") return;

      var x, y;
      x = (e.currentTarget.getXY()[0] - e.pageX + (e.currentTarget.get('offsetWidth') / 2)) / 8;
      y = (e.currentTarget.getXY()[1] - e.pageY + (e.currentTarget.get('offsetHeight') / 2)) / 8;
      x = Math.round(x);
      y = Math.round(y);

      e.currentTarget.one('img').tick({
        top: y + parseInt(e.currentTarget.getAttribute('original-top')),
        left: x + parseInt(e.currentTarget.getAttribute('original-left')) 
      });
    },

    _onMouseLeave: function(e) {
      clearTimeout(e.currentTarget.getAttribute('overlayDelay'));
      clearInterval(e.currentTarget.getAttribute('tickInterval'));

      e.currentTarget.removeClass('hovering');
      var img = e.currentTarget.one('img');

      if (e.currentTarget.getAttribute('original-top')) {
        img.setStyle('top', e.currentTarget.getAttribute('original-top'));
      }
      if (e.currentTarget.getAttribute('original-left')) {
        img.setStyle('left', e.currentTarget.getAttribute('original-left'));
      }
    },

  });
});

</script>