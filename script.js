function add_menu() {


var host	= window.location.host;
var protocol	= window.location.protocol;
var mainUrl	= protocol + "//" + host;

var cUrl	= decodeURIComponent(window.location.href);
cUrl		= cUrl.replace("http://","");
var path_arr	= cUrl.split("/");

if(!path_arr[path_arr.length-1]) {
  path_arr.splice(path_arr.length-1,1);
}


/* -- desktop -- */
$("#nav li.index-collection a").each(function() {
 var self = $(this);
 var jsonUrl = mainUrl + $(this).attr("href") + "?format=json";

 self.parent().removeAttr("class").addClass("folders-collection folder");

 /* -- set color -- */
 var curr_loc = "";
 
 if(path_arr.indexOf("#") == -1) {
   switch (path_arr.length) {
	case 1: curr_loc = "/"
		break
	case 2: curr_loc = "/" + path_arr[path_arr.length-1] + "/"
		break
   }
   if(self.attr("href") == curr_loc) {
     self.parent().addClass("active-link");
   }
 }
 /* -- set color -- */

 $.getJSON(jsonUrl, function(data) {
   var coll_arr		= data.collection.collections;
   var submenu		= $('<div />').attr("class", "subnav");
   var submenu_ul	= $('<ul />');

   $.each(coll_arr, function(key, value) {
     var submenu_li = $("<li />").attr("class","page-collection");
     $("<a />").attr("class","transition-link").attr("href",self.attr("href") + "#" + value.fullUrl).text(value.navigationTitle).appendTo(submenu_li);

     if(path_arr[path_arr.indexOf("#")+1] == value.fullUrl.replace(/\//g,"")) {
       submenu_li.addClass("active-link");
       self.parent().addClass("active-link");
     }

     submenu_li.appendTo(submenu_ul);
   });

   submenu_ul.appendTo(submenu);
   submenu.insertAfter(self);
 });

});
/* -- desktop -- */


$('#nav a[href$="/#/' + path_arr[path_arr.length-1] + '/"]').parent().parent().find("li").removeClass("active-link");
$('#nav a[href$="/#/' + path_arr[path_arr.length-1] + '/"]').parent().parent().parent().parent().addClass("active-link");
$('#nav a[href$="/#/' + path_arr[path_arr.length-1] + '/"]').parent().addClass("active-link");


$('#nav a[href$="/#/' + path_arr[path_arr.length-1] + '/"]').each(function() {
  $("#nav .external-link a[target='_blank']").parent().parent().parent().parent().removeClass("active-link");
});

$("#nav .external-link a").parent().removeClass("active-link");
$("#nav .external-link a[href$='/" + path_arr[path_arr.length-1] + "'][target!='_blank']").parent().parent().find("li").removeClass("active-link");
$("#nav .external-link a[href$='/" + path_arr[path_arr.length-1] + "'][target!='_blank']").parent().addClass("active-link");
$("#nav .external-link a[href$='/" + path_arr[path_arr.length-1] + "'][target!='_blank']").parent().parent().parent().parent().addClass("active-link");


$("#nav .external-link a[target='_blank']").click(function() { 
  $("#nav li").removeClass("active-link");
  $(this).parent().parent().parent().parent().addClass("active-link");
  $(this).parent().addClass("active-link");
});

}

function title_head() {
  $("#navigator h2.logo-subtitle").each(function (){
    $(this).text($(this).text().replace(/[^A-Za-z, ]/g,""));
  });

  $("#navigator h2.logo-subtitle").css("text-align", "center");
}

function amazon_links() {
  $("#container a.product-title").each(function (){
   $(this).removeAttr("class");
  });
}

$("#project").bind("DOMSubtreeModified", function(objEvent) {
   amazon_links();
});


$(document).ready(function(){
  add_menu();
  title_head();
  amazon_links();
});


$(window).on('hashchange', function(){
  add_menu();
});